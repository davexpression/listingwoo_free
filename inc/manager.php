<?php

/**
 * Enables Automatic Updates
 *
 * @package    ListingWoo
 * @author     David Towoju (Figarts) <hello@figarts.co>
 */
class ListingWoo_Manager {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $theme_name    The ID of this plugin.
	 */
	private $theme_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $theme_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $theme_name, $version ) {

		$this->theme_name = $theme_name;
		$this->version = $version;

    // This is the secret key for API authentication.
    define('LISTINGWOO_SECRET_KEY', '5a38f98c733a14..33353372'); 
    
    // This is the URL where API query request will be sent to.
    define('LISTINGWOO_LICENSE_SERVER_URL', 'https://figarts.co'); 

    // This is a value to  identify item/product.
    define('LISTINGWOO_ITEM_REFERENCE', $this->theme_name ); 

    define('LISTINGWOO_LICENSE_KEY', ( is_multisite() ? get_site_option('listingwoo_license_key') : get_option('listingwoo_license_key') ) ); 

    /* Updater Config */
    $this->config = array(
      'server'  => LISTINGWOO_LICENSE_SERVER_URL,
      'type'    => 'theme',
      'id'      => 'listingwoo',
      'api'     => '1.0.0',
      'post'    => array('figarts_action' => 'license_check', 'secret_key' => LISTINGWOO_SECRET_KEY, 'license_key' => LISTINGWOO_LICENSE_KEY, 'license_server' => LISTINGWOO_LICENSE_SERVER_URL, 'multisite' => is_multisite() ),
    );

    /* Admin Init */
    add_action( 'admin_init', array( $this, 'listingwoo_init' ) );
    add_action('admin_menu', array( $this, 'listingwoo_license_menu' ), 11 );

    /* Fix Install Folder */
    add_filter( 'upgrader_post_install', array( $this, 'fix_install_folder' ), 11, 3 );

  }


  /**
   * Admin Init.
   * Some functions only available in admin.
   */
  public function listingwoo_init(){

    /* Add theme update data */
    if( 'plugin' !== $this->config['type'] ){
      // ('sdsds');

      add_filter( 'pre_set_site_transient_update_themes', array( $this, 'add_theme_update_data' ), 10, 2 );
    }

    /* Add plugin update data */
    if( 'theme' !== $this->config['type'] ){
      add_filter( 'pre_set_site_transient_update_plugins', array( $this, 'add_plugin_update_data' ), 10, 2 );
    }

    /* Plugin Information */
    if( 'theme' !== $this->config['type'] ){
      add_filter( 'plugins_api_result', array( $this, 'plugin_info' ), 10, 3 );
    }
  }

  /**
   * Add theme update data if available
   */
  public function add_theme_update_data( $value, $transient ){
    if( isset( $value->response ) ){
      $update_data = $this->get_data( 'query_themes' );
      foreach( $update_data as $theme => $data ){
        if( isset( $data['new_version'], $data['theme'], $data['url'] ) ){
          $value->response[$theme] = (array) $data;
        }
        else{
          unset( $value->response[$theme] );
        }
      }
    }
    return $value;
  }

  /**
   * Add plugin update data if available
   */
  public function add_plugin_update_data( $value, $transient ){
    if( isset( $value->response ) ){
      $update_data = $this->get_data( 'query_plugins' );
          // wp_dump($update_data);

      foreach( $update_data as $plugin => $data ){
        if( isset( $data['new_version'], $data['slug'], $data['plugin'] ) ){
          $value->response[$plugin] = (object)$data;
        }
        else{
          unset( $value->response[$plugin] );
        }
      }
    }
    return $value;
  }

  /**
   * Plugin Information
   */
  public function plugin_info( $res, $action, $args ){

    /* Get list plugin */
    if( 'group' == $this->config['type'] ){
      $list_plugins = $this->get_data( 'list_plugins' );
    }
    else{
      $slug = dirname( $this->config['id'] );
      $list_plugins = array(
        $slug => $this->config['id'],
      );
    }

    /* If in our list, add our data. */
    if( 'plugin_information' == $action && isset( $args->slug ) && array_key_exists( $args->slug, $list_plugins ) ){

      $info = $this->get_data( 'plugin_information', $list_plugins[$args->slug] );

      if( isset( $info['name'], $info['slug'], $info['external'], $info['sections'] ) ){
        $res = (object)$info;
      }
    }
    return $res;
  }

  /**
   * Get update data from server
   */
  public function get_data( $action, $plugin = '' ){

    /* Get WP Version */
    global $wp_version;

    /* Remote Options */
    $body = $this->config['post'];
    if( 'query_plugins' == $action ){
      $body['plugins'] = get_plugins();
    }
    elseif( 'query_themes' == $action ){
      $themes = array();
      $get_themes = wp_get_themes();
      foreach( $get_themes as $theme ){
        $stylesheet = $theme->get_stylesheet();
        $themes[$stylesheet] = array(
          'Name' => $theme->get( 'Name' ),
          'ThemeURI' => $theme->get( 'ThemeURI' ),
          'Description' => $theme->get( 'Description' ),
          'Author' => $theme->get( 'Author' ),
          'AuthorURI' => $theme->get( 'AuthorURI' ),
          'Version' => $theme->get( 'Version' ),
          'Template' => $theme->get( 'Template' ),
          'Status' => $theme->get( 'Status' ),
          'Tags' => $theme->get( 'Tags' ),
          'TextDomain' => $theme->get( 'TextDomain' ),
          'DomainPath' => $theme->get( 'DomainPath' ),
        );
      }
      $body['themes'] = $themes;
    }
    elseif( 'plugin_information' == $action ){
      $body['plugin'] =  $plugin;
    }
    $options = array(
      'timeout'    => 20,
      'body'       => $body,
      'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo( 'url' ),
    ); 

    /* Remote URL */
    $url_args = array(
      'fx_updater'          => $action,
      $this->config['type'] => $this->config['id'],
    );
    $server = set_url_scheme( $this->config['server'], 'http' );
    $url = $http_url = add_query_arg( $url_args, $server );
    if ( $ssl = wp_http_supports( array( 'ssl' ) ) ){
      $url = set_url_scheme( $url, 'https' );
    }

    /* Try HTTPS */
    $raw_response = wp_remote_post( esc_url_raw( $url ), $options );

    /* Fail, try HTTP */
    if ( is_wp_error( $raw_response ) ) {
      $raw_response = wp_remote_post( esc_url_raw( $http_url ), $options );
    }
    // wp_dump($raw_response);

    /* Still fail, bail. */
    if ( is_wp_error( $raw_response ) || 200 != wp_remote_retrieve_response_code( $raw_response ) ) {
      return array();
    }

    /* return array */
    $data = json_decode( trim( wp_remote_retrieve_body( $raw_response ) ), true );
    return is_array( $data ) ? $data : array();
  }

  /**
   * Fix Install Folder
   */
  public function fix_install_folder( $true, $hook_extra, $result ){
    if ( isset( $hook_extra['plugin'] ) ){
      global $wp_filesystem;
      $proper_destination = trailingslashit( $result['local_destination'] ) . dirname( $hook_extra['plugin'] );
      $wp_filesystem->move( $result['destination'], $proper_destination );
      $result['destination'] = $proper_destination;
      $result['destination_name'] = dirname( $hook_extra['plugin'] );
      global $hook_suffix;
      if( 'update.php' == $hook_suffix && isset( $_GET['action'], $_GET['plugin'] ) && 'upgrade-plugin' == $_GET['action'] && $hook_extra['plugin'] == $_GET['plugin'] ){
        activate_plugin( $hook_extra['plugin'] );
      }
    }
    elseif( isset( $hook_extra['theme'] ) ){
      global $wp_filesystem;
      $proper_destination = trailingslashit( $result['local_destination'] ) . $hook_extra['theme'];
      $wp_filesystem->move( $result['destination'], $proper_destination );
      if( get_option( 'theme_switched' ) == $hook_extra['theme'] && $result['destination_name'] == get_stylesheet() ){
        wp_clean_themes_cache();
        switch_theme( $hook_extra['theme'] );
      }
      $result['destination'] = $proper_destination;
      $result['destination_name'] = $hook_extra['theme'];
    }
    return $true;
  }


  /**
   * Licence Menu
   */
  public function listingwoo_license_menu() {
    add_theme_page( 'ListingWoo', 'ListingWoo', 'manage_options', 'listingwoo', array($this, 'listingwoo_license_management_page') );
  }


  /**
   * Licence Menu Settings
   */
  public function listingwoo_license_management_page() {
    echo '<div class="wrap">';
    echo '<h2>' .  esc_html__('ListingWoo - perfect integration of WooCommerce with ListingPro') .'</h2>';

    /*** License activate button was clicked ***/
    if (isset($_REQUEST['listingwoo_activate_license'])) {
        $license_key = $_REQUEST['listingwoo_license_key'];
        // API query parameters
        $api_params = array(
          'slm_action' => 'slm_activate',
          'secret_key' => LISTINGWOO_SECRET_KEY,
          'license_key' => $license_key,
          'registered_domain' => $_SERVER['SERVER_NAME'],
          'item_reference' => urlencode(LISTINGWOO_ITEM_REFERENCE),
        );

          // Send query to the license manager server
          $query = esc_url_raw(add_query_arg($api_params, LISTINGWOO_LICENSE_SERVER_URL));
          $response = wp_remote_get($query, array('timeout' => 20, 'sslverify' => false));
          // Check for error in the response

          if (is_wp_error($response)){
              $output = "Unexpected Error! The query returned with an error.";
          }
          //var_dump($response);//uncomment it if you want to look at the full response
          
          // License data.
          $license_data = json_decode(wp_remote_retrieve_body($response));
          
          // TODO - Do something with it.
          //var_dump($license_data);//uncomment it to look at the data
          
          if($license_data->result == 'success'){//Success was returned for the license activation
              
              //Uncomment the followng line to see the message that returned from the license server
              $output = '<br /> '.$license_data->message;
              
              //Save the license key in the options table
              if( is_multisite() ){
                update_site_option('listingwoo_license_key', $license_key);
              }
              else{
                update_option('listingwoo_license_key', $license_key); 
              }
          }
          else{
              //Show error to the user. Probably entered incorrect license key.
              
              //Uncomment the followng line to see the message that returned from the license server
              $output = '<br /> '.$license_data->message;
          }
      }
      /*** End of license activation ***/
      
      /*** License activate button was clicked ***/
      if (isset($_REQUEST['listingwoo_deactivate_license'])) {
          $license_key = $_REQUEST['listingwoo_license_key'];
          // API query parameters
          $api_params = array(
              'slm_action' => 'slm_deactivate',
              'secret_key' => LISTINGWOO_SECRET_KEY,
              'license_key' => $license_key,
              'registered_domain' => $_SERVER['SERVER_NAME'],
              'item_reference' => urlencode(LISTINGWOO_ITEM_REFERENCE),
          );
          // Send query to the license manager server
          $query = esc_url_raw(add_query_arg($api_params, LISTINGWOO_LICENSE_SERVER_URL));
          $response = wp_remote_get($query, array('timeout' => 20, 'sslverify' => false));
          // Check for error in the response
          if (is_wp_error($response)){
              $output = "Unexpected Error! The query returned with an error.";
          }
          //var_dump($response);//uncomment it if you want to look at the full response
          
          // License data.
          $license_data = json_decode(wp_remote_retrieve_body($response));
          
          // TODO - Do something with it.
          //var_dump($license_data);//uncomment it to look at the data
          
          if($license_data->result == 'success'){//Success was returned for the license activation
              
              //Uncomment the followng line to see the message that returned from the license server
              $output =  '<br />' .$license_data->message;
              
              //Remove the licensse key from the options table. It will need to be activated again.
              if( is_multisite() ){
                update_site_option('listingwoo_license_key', '');
              }
              else{
                update_option('listingwoo_license_key', ''); 
              }
          }
          else{
              //Show error to the user. Probably entered incorrect license key.
              
              //Uncomment the followng line to see the message that returned from the license server
              $output =  '<br />' .$license_data->message;
          }
          
      }
      /*** End of sample license deactivation ***/
      
      ?>
      

      <div class="metabox-holder">
        <div class="postbox">
          <div class="inside">

            <?php printf( '<p> %s <a href="%s" class="link" target="_blank">%s</a> </p>', esc_html__('Enter your licence key to be able to receive updates and support. You can get it in '), esc_url( 'https://figarts.co/my-account/licenses/' ), esc_html__( 'My Account' ) ); ?>

            <span style="font-weight: bold; color: green"><?php echo ( isset($output) && !empty($output) ) ? $output : ''; ?></span>
            <?php
              // Load current license after query might have affected it from above
              $activated = false;

              $license =  is_multisite() ? get_site_option('listingwoo_license_key') : get_option('listingwoo_license_key');
              $api_params = array(
                'slm_action' => 'slm_check',
                'secret_key' => LISTINGWOO_SECRET_KEY,
                'license_key' => $license,
              );
              // Send query to the license manager server
              $query = esc_url_raw(add_query_arg($api_params, LISTINGWOO_LICENSE_SERVER_URL));
              $response = wp_remote_get($query, array('timeout' => 20, 'sslverify' => false));
              if (!is_wp_error($response)){
                $license_check = json_decode(wp_remote_retrieve_body($response));
                if ( $license_check->result == 'success' || $license_check->status == 'active' )
                  $activated = true;
              }
            ?>
            <form action="" method="post">
              <table class="form-table">
                <tr>
                  <th style="width:100px;"><label for="listingwoo_license_key">License Key</label></th>
                  <td ><input class="regular-text" type="text" id="listingwoo_license_key" name="listingwoo_license_key"  value="<?php echo $license; ?>" ></td>
                </tr>
              </table>
              <p class="submit">
                <input type="submit" name="listingwoo_activate_license" value="Activate" class="button-primary" <?php if((isset($activated)) && $activated == true ) echo 'disabled="disabled"'; ?> />
                <input type="submit" name="listingwoo_deactivate_license" value="Deactivate" class="button" />
              </p>
            </form>

          </div><!-- .inside -->
        </div><!-- .postbox -->

      </div><!-- .metabox-holder -->  

      <?php
      
      echo '</div>';
  }

}
$manager = new ListingWoo_Manager( LISTINGWOO_NAME, LISTINGWOO_VERSION );