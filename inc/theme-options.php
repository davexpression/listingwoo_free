<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package ListingWoo_WooCommerce
 */

 function listingwoo_add_section($sections){
  $sections[] = array(
    'title' => esc_html__('ListingWoo', 'listingwoo'),
    'desc' => '<p class="description">'.esc_html__('Thanks for buying ListingWoo. Please customize as you like', 'listingwoo').'</p>',
    'class' => 'listingwoo_redux_menu',
    'icon' => 'dashicons-before dashicons-cart',
    'fields' => listingwoo_redux_fields()
  );

  return $sections;
}
add_filter("redux/options/listingpro_options/sections", 'listingwoo_add_section');


function listingwoo_redux_fields(){

  $fields = array(
    array(
      'id'       => 'listingwoo_redux_product_types',
      'type'     => 'checkbox',
      'title'    => esc_html__( 'Woo Product Types', 'listingwoo' ),
      'desc' => esc_html__( 'Only supported WooCommerce Product Types will appear in Dashboard and Product pages', 'listingwoo' ),
      'subtitle' => '',
      'options'  => wc_get_product_types(),
      'default'  => array(
        'simple' => '1',
        'variable' => '1',
        'external' => '1',
        'grouped' => '1',
        'booking' => '1',
        'accommodation-booking' => '1', 
      ),
      // 'default' => array_fill_keys( array_keys(wc_get_product_types()), '1')
    ),
    // array(
    //   'id'       => 'listingwoo_redux_disable_guest',
    //   'type'     => 'text',
    //   'title'    => esc_html__( 'Disable Guest Posting', 'listingwoo' ),
    //   'subtitle' => esc_html__( 'No guest should create listings', 'listingwoo' ),
    //   'desc'     => esc_html__( 'Redirect guests here. Empty means no redirection', 'listingwoo' ),
    //   'default'  => home_url()// 1 = on | 0 = off
    // ),
    array(
      'id'       => 'listingwoo_redux_autocreate_product',
      'type'     => 'button_set',
      'title'    => esc_html__( 'Auto-Create Products', 'listingwoo' ),
      'subtitle' => esc_html__( 'when a listing is published', 'listingwoo' ),
      'options' => array(
        '1' => 'No', 
        '2' => 'Yes, but product\'s status is pending', 
        '3' => 'Yes'
      ), 
      'default' => '2'
    ), 
    array(
      'id'       => 'listingwoo_redux_product_freeplan',
      'type'     => 'checkbox',
      'title'    => esc_html__( 'No Product for Free Plans', 'listingwoo' ),
      'subtitle' => esc_html__( 'works if WooCommerce Product Vendors is active', 'listingwoo' ),
      'desc'     => 'Diable product creation for free listings',
      'default'  => '0'// 1 = on | 0 = off
    ),
    array(
      'id'       => 'listingwoo_redux_redirect_product',
      'type'     => 'checkbox',
      'title'    => esc_html__( 'Redirect products to listings', 'listingwoo' ),
      'subtitle' => esc_html__( 'Only works if product is linked to listing', 'listingwoo' ),
      'desc'     => '',
      'default'  => '0'// 1 = on | 0 = off
    ),
    array(
      'id'       => 'listingwoo_redux_reservation_time',
      'type'     => 'text',
      'title'    => esc_html__( 'Booking Reservation Time', 'listingwoo' ),
      'subtitle' => esc_html__( 'Works if WooCommerce Booking is active', 'listingwoo' ),
      'desc'     => esc_html__( 'in minutes', 'listingwoo' ),
      'validate' => 'numeric',
      'default'  => '60',
    ),
    array(
      'id'       => 'listingwoo_redux_show_listings_admin',
      'type'     => 'checkbox',
      'title'    => esc_html__( 'Show Admin All Listings', 'listingwoo' ),
      'subtitle' => esc_html__( 'in the listing author dashboard', 'listingwoo' ),
      'desc'     => 'Admin can see integrated listings of other authors',
      'default'  => '1'// 1 = on | 0 = off
    ),
    array(
      'id'       => 'listingwoo_redux_create_vendors',
      'type'     => 'checkbox',
      'title'    => esc_html__( 'Make Subscribers Product Vendors', 'listingwoo' ),
      'subtitle' => esc_html__( 'works if WooCommerce Product Vendors is active', 'listingwoo' ),
      'desc'     => esc_html__( 'upon registration', 'listingwoo'),
      'default'  => '1'// 1 = on | 0 = off
    ),
    array(
      'id'       => 'listingwoo_redux_commission',
      'type'     => 'text',
      'title'    => esc_html__( 'Product Commission %', 'listingwoo' ),
      'subtitle' => esc_html__( 'works if WooCommerce Product Vendors is active', 'listingwoo' ),
      'desc'     => esc_html__( 'This is the commission amount the vendor will receive. Product level commission can be set which will override this commission.', 'listingwoo' ),
      'validate' => 'numeric',
      'default'  => '60',
    ),
    array(
      'id'       => 'listingwoo_redux_admin_email',
      'type'     => 'text',
      'title'    => esc_html__( 'Product Admin Email', 'listingwoo' ),
      'subtitle' => esc_html__( 'Must be a Vendor with Vendor Admin role', 'listingwoo' ),
      'desc'     => esc_html__( 'This is the user that will manage vendor products in addition to Administrator.', 'listingwoo' ),
      'validate' => 'email',
      'default'  => get_option( 'admin_email' ),
    ),


  );

  

  return $fields;
}
