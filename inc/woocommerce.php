<?php
/**
 * WooCommerce Compatibility File
 *
 * @link https://woocommerce.com/
 *
 * @package ListingWoo_WooCommerce
 */

/**
 * WooCommerce setup function.
 *
 * @link https://docs.woocommerce.com/document/third-party-custom-theme-compatibility/
 * @link https://github.com/woocommerce/woocommerce/wiki/Enabling-product-gallery-features-(zoom,-swipe,-lightbox)-in-3.0.0
 *
 * @return void
 */
function listingwoo_woocommerce_setup() {
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}
add_action( 'after_setup_theme', 'listingwoo_woocommerce_setup' );

/**
 * WooCommerce specific scripts & stylesheets.
 *
 * @return void
 */
function listingwoo_woocommerce_scripts() {
	wp_enqueue_style( 'listingwoo-woocommerce-style', get_stylesheet_directory_uri() . '/woocommerce.css' );

  $font_path   = WC()->plugin_url() . '/assets/fonts/';
	$inline_font = '@font-face {
			font-family: "star";
			src: url("' . $font_path . 'star.eot");
			src: url("' . $font_path . 'star.eot?#iefix") format("embedded-opentype"),
				url("' . $font_path . 'star.woff") format("woff"),
				url("' . $font_path . 'star.ttf") format("truetype"),
				url("' . $font_path . 'star.svg#star") format("svg");
			font-weight: normal;
			font-style: normal;
		}';

	wp_add_inline_style( 'listingwoo-woocommerce-style', $inline_font );
}
add_action( 'wp_enqueue_scripts', 'listingwoo_woocommerce_scripts' );

/**
 * Disable the default WooCommerce stylesheet.
 *
 * Removing the default WooCommerce stylesheet and enqueing your own will
 * protect you during WooCommerce core updates.
 *
 * @link https://docs.woocommerce.com/document/disable-the-default-stylesheet/
 */
// add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

/**
 * Add 'woocommerce-active' class to the body tag.
 *
 * @param  array $classes CSS classes applied to the body tag.
 * @return array $classes modified to include 'woocommerce-active' class.
 */
function listingwoo_woocommerce_active_body_class( $classes ) {
	$classes[] = 'woocommerce-active';

	return $classes;
}
add_filter( 'body_class', 'listingwoo_woocommerce_active_body_class' );

/**
 * Products per page.
 *
 * @return integer number of products.
 */
function listingwoo_woocommerce_products_per_page() {
	return 12;
}
add_filter( 'loop_shop_per_page', 'listingwoo_woocommerce_products_per_page' );

/**
 * Product gallery thumnbail columns.
 *
 * @return integer number of columns.
 */
function listingwoo_woocommerce_thumbnail_columns() {
	return 4;
}
add_filter( 'woocommerce_product_thumbnails_columns', 'listingwoo_woocommerce_thumbnail_columns' );

/**
 * Default loop columns on product archives.
 *
 * @return integer products per row.
 */
function listingwoo_woocommerce_loop_columns() {
	return 3;
}
add_filter( 'loop_shop_columns', 'listingwoo_woocommerce_loop_columns' );

/**
 * Related Products Args.
 *
 * @param array $args related products args.
 * @return array $args related products args.
 */
function listingwoo_woocommerce_related_products_args( $args ) {
	$defaults = array(
		'posts_per_page' => 3,
		'columns'        => 3,
	);

	$args = wp_parse_args( $defaults, $args );

	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'listingwoo_woocommerce_related_products_args' );

if ( ! function_exists( 'listingwoo_woocommerce_product_columns_wrapper' ) ) {
	/**
	 * Product columns wrapper.
	 *
	 * @return  void
	 */
	function listingwoo_woocommerce_product_columns_wrapper() {
		$columns = listingwoo_woocommerce_loop_columns();
		echo '<div class="columns-' . absint( $columns ) . '">';
	}
}
add_action( 'woocommerce_before_shop_loop', 'listingwoo_woocommerce_product_columns_wrapper', 40 );

if ( ! function_exists( 'listingwoo_woocommerce_product_columns_wrapper_close' ) ) {
	/**
	 * Product columns wrapper close.
	 *
	 * @return  void
	 */
	function listingwoo_woocommerce_product_columns_wrapper_close() {
		echo '</div>';
	}
}
add_action( 'woocommerce_after_shop_loop', 'listingwoo_woocommerce_product_columns_wrapper_close', 40 );

/**
 * Remove default WooCommerce wrapper.
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );

if ( ! function_exists( 'listingwoo_woocommerce_wrapper_before' ) ) {
	/**
	 * Before Content.
	 *
	 * Wraps all WooCommerce content in wrappers which match the theme markup.
	 *
	 * @return void
	 */
	function listingwoo_woocommerce_wrapper_before() {
		?>
    <div class="container">
      <div class="row">
        <div id="primary" class="section-contianer col-md-8 col-sm-8">
    			<main id="main" class="post-content" role="main">
		<?php
	}
}
add_action( 'woocommerce_before_main_content', 'listingwoo_woocommerce_wrapper_before' );

if ( ! function_exists( 'listingwoo_woocommerce_wrapper_after' ) ) {
	/**
	 * After Content.
	 *
	 * Closes the wrapping divs.
	 *
	 * @return void
	 */
	function listingwoo_woocommerce_wrapper_after() {
		?>
			</main><!-- #main -->
		</div><!-- #primary -->
		<?php
	}
}
add_action( 'woocommerce_after_main_content', 'listingwoo_woocommerce_wrapper_after' );

/**
 * Sample implementation of the WooCommerce Mini Cart.
 *
 * You can add the WooCommerce Mini Cart to header.php like so ...
 *
	<?php
		if ( function_exists( 'listingwoo_woocommerce_header_cart' ) ) {
			listingwoo_woocommerce_header_cart();
		}
	?>
 */

if ( ! function_exists( 'listingwoo_woocommerce_cart_link_fragment' ) ) {
	/**
	 * Cart Fragments.
	 *
	 * Ensure cart contents update when products are added to the cart via AJAX.
	 *
	 * @param array $fragments Fragments to refresh via AJAX.
	 * @return array Fragments to refresh via AJAX.
	 */
	function listingwoo_woocommerce_cart_link_fragment( $fragments ) {
		ob_start();
		listingwoo_woocommerce_cart_link();
		$fragments['a.cart-contents'] = ob_get_clean();

		return $fragments;
	}
}
add_filter( 'woocommerce_add_to_cart_fragments', 'listingwoo_woocommerce_cart_link_fragment' );

if ( ! function_exists( 'listingwoo_woocommerce_cart_link' ) ) {
	/**
	 * Cart Link.
	 *
	 * Displayed a link to the cart including the number of items present and the cart total.
	 *
	 * @return void
	 */
	function listingwoo_woocommerce_cart_link() {
		?>
			<a class="cart-contents" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'listingwoo' ); ?>">
				<?php /* translators: number of items in the mini cart. */ ?>
				<span class="amount"><?php echo wp_kses_data( WC()->cart->get_cart_subtotal() ); ?></span> <span class="count"><?php echo wp_kses_data( sprintf( _n( '%d item', '%d items', WC()->cart->get_cart_contents_count(), 'listingwoo' ), WC()->cart->get_cart_contents_count() ) );?></span>
			</a>
		<?php
	}
}

if ( ! function_exists( 'listingwoo_woocommerce_header_cart' ) ) {
	/**
	 * Display Header Cart.
	 *
	 * @return void
	 */
	function listingwoo_woocommerce_header_cart() {
		if ( is_cart() ) {
			$class = 'current-menu-item';
		} else {
			$class = '';
		}
		?>
		<ul id="site-header-cart" class="site-header-cart">
			<li class="<?php echo esc_attr( $class ); ?>">
				<?php listingwoo_woocommerce_cart_link(); ?>
			</li>
			<li>
				<?php
					$instance = array(
						'title' => '',
					);

					the_widget( 'WC_Widget_Cart', $instance );
				?>
			</li>
		</ul>
		<?php
	}
}


/**
 * Display Cart in Sidebar
 *
 * @return void
 */
function listingwoo_woocommerce_hookify() {

  // Title
  add_action('listingwoo_woocommerce_single_product_summary', 'listingwoo_show_title');

  // Price
  add_action('listingwoo_woocommerce_single_product_summary', 'woocommerce_template_single_price');

  // Cart 
  add_action('listingwoo_woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart');

}
add_action( 'wp', 'listingwoo_woocommerce_hookify', 10 );


/**
 * Show title of lisint instead of title of product
 *
 */
function listingwoo_show_title(){

  global $listingwoo_listing_id;

  $prefix = esc_html__('Buy ', 'listingwoo');
  $title = get_the_title( $listingwoo_listing_id );
  $product_id = get_the_id();

  $_product = wc_get_product( $product_id );
  
  if( $_product->is_type( 'booking' ) || $_product->is_type( 'accommodation-booking' ) ) {
    $prefix = esc_html__('Reserve ', 'listingwoo');
  }

  echo '<h3 class="widget-title">' . $prefix .' '. $title .'</h3>';
}

function listingwoo_get_listing_id(){
  global $listingwoo_listing_id;
  $listingwoo_listing_id = get_the_ID();
}
add_action( 'wp', 'listingwoo_get_listing_id', 1 );
