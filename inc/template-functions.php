<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package ListingWoo
 */

/**
 * Get woocommerce products.
 */
function listingwoo_get_woocommerce_products(){

  $listingpro_options = get_option('listingpro_options');
  $supported_types = array_filter($listingpro_options['listingwoo_redux_product_types']);

  $query_args = array(
   'post_type' => 'product',
   'posts_per_page'   => -1,
   'tax_query' => array(
      array(
        'taxonomy' => 'product_type',
        'field'    => 'slug',
        'terms'    => array_keys($supported_types), // Limit items to supported products
      ),
    ),
	 );

  // Show products created by listing authors alone, if admin, show all products 
   if ( !current_user_can( 'manage_options' )) {
     $query_args['author'] = get_current_user_id();
   }

	$my_query = new WP_Query($query_args);
	return $my_query;
}


/**
 * Remove unsupported product types
 */
function listingwoo_remove_product_types( $types ){

  if ( is_admin() && isset($_GET['page']) && $_GET['page'] == 'ListingWoo' ) 
    return $types;

  $listingpro_options = get_option('listingpro_options');
  $supported_types = array_filter($listingpro_options['listingwoo_redux_product_types']);
   foreach ($types as $key => $value) {
     if ( ! in_array($key, array_keys($supported_types) ) ){
        unset( $types[$key] );
     }
   }
  return $types;
}

function listingwoo_remove_product_types_initwrapper(){
  add_filter( 'product_type_selector', 'listingwoo_remove_product_types', 99 );
}
add_action('init', 'listingwoo_remove_product_types_initwrapper');



/**
 * Maybe Create a product after a listing is published
 */
function listingwoo_autocreate_products( $new_status, $old_status, $post ){

  $options = get_option('listingpro_options');
  if ( $options['listingwoo_redux_autocreate_product'] == '1' || LISTINGWOO_WC == false )
    return;

  //  No product creation for free product
  if ($options['listingwoo_redux_product_freeplan'] == '1'){
    $post_meta = get_post_meta( $post->ID, 'lp_listingpro_options', true );
    $plan_id = $post_meta['Plan_id'];
    $plan_price = get_post_meta( $plan_id, 'plan_price', true );
    if ( (!is_numeric($plan_price) || $plan_price <= 0) )
      return;
  }

  if( 'publish' == $new_status && 'publish' != $old_status && $post->post_type == 'listing' ) {

  	// Check if a product is integrated already
  	$linked = get_post_meta($post->ID, '_wc_product_id_lw', true);
  	if (!empty($linked))
  		return;

  	$id = $post->ID;
  	$title = $post->post_title;
  	$author_id = $post->post_author;

    $args = array(	    
			'post_author' => $post->post_author,
	    'post_content' => $post->post_content,
	    'post_status' => "pending",
	    'post_title' => 'Product - '.$post->post_title,
	    'post_parent' => '',
	    'post_type' => "product",
		);

    if ( $options['listingwoo_redux_autocreate_product'] == '3' )
      $args['post_status'] == 'published';

		$product_id = wp_insert_post( $args );

    // make booking the default
		wp_set_object_terms( $product_id, 'booking', 'product_type' );

    // Add product key to post and vice versa (useful for redirection)
    update_post_meta( $post->ID, '_wc_product_id_lw', $product_id);
    update_post_meta( $product_id, '_listingwoo_id', $post->ID);
    
		update_post_meta( $product_id, '_visibility', 'visible' );
		update_post_meta( $product_id, '_stock_status', 'instock');
		update_post_meta( $product_id, 'total_sales', '0' );
		update_post_meta( $product_id, '_downloadable', 'no' );
		update_post_meta( $product_id, '_virtual', 'yes' );
		update_post_meta( $product_id, '_regular_price', '' );
		update_post_meta( $product_id, '_sale_price', '' );
		update_post_meta( $product_id, '_purchase_note', '' );
		update_post_meta( $product_id, '_featured', 'no' );
		update_post_meta( $product_id, '_weight', '' );
		update_post_meta( $product_id, '_length', '' );
		update_post_meta( $product_id, '_width', '' );
		update_post_meta( $product_id, '_height', '' );
		update_post_meta( $product_id, '_sku', '' );
		update_post_meta( $product_id, '_product_attributes', array() );
		update_post_meta( $product_id, '_sale_price_dates_from', '' );
		update_post_meta( $product_id, '_sale_price_dates_to', '' );
		update_post_meta( $product_id, '_price', '' );
		update_post_meta( $product_id, '_sold_individually', '' );
		update_post_meta( $product_id, '_manage_stock', 'no' );
		update_post_meta( $product_id, '_backorders', 'no' );
		update_post_meta( $product_id, '_stock', '' );
  
		// Get vendor term
		$term = listingwoo_get_vendor_term_by_id($author_id);
		// Then we can set the taxonomy
		if ( !empty($term) ) {
			wp_set_object_terms( $product_id, (int)$term->term_id, WC_PRODUCT_VENDORS_TAXONOMY, true );
		}
  
  }
}
add_action( 'transition_post_status', 'listingwoo_autocreate_products', 10, 3 );


/**
 * Maybe Redirect product views to listing if not admin
 */
function listingwoo_redirect_products( ){
  
  if ( ! is_singular( 'product' ) )
    return;
  
  $options = get_option('listingpro_options');
  if ( $options['listingwoo_redux_redirect_product'] == '0' )
    return;

  $meta = get_post_meta( get_the_ID(), '_listingwoo_id', true );
  if ( isset($meta) && is_numeric($meta)  ) {
    wp_redirect( get_page_link( $meta ), 301 );
    exit;
  }
}
add_action( 'template_redirect', 'listingwoo_redirect_products' );


/**
 * Maybe Set bookable product reservation time
 */
function listingwoo_redux_reservation_time( $minute ){
  
  if ( ! class_exists( 'WC_Bookings' ) )
    return $minute;
  
  $options = get_option('listingpro_options');
  if ( ! is_numeric( $options['listingwoo_redux_reservation_time'] ) )
    return $minute;

  $minute = $options['listingwoo_redux_reservation_time'];
  return $minute;
}
add_filter( 'woocommerce_bookings_remove_inactive_cart_time', 'listingwoo_redux_reservation_time' );

/**
 * Customize add to cart message
 */
function listingwoo_custom_add_to_cart_message($message, $product_id = null) {
  $titles[] = get_the_title( key($product_id));
  $titles = array_filter( $titles );
  $added_text = sprintf( _n( '%s has been added to cart.', '%s have been added to cart', sizeof( $titles ), 'listingwoo' ), wc_format_list_of_items( $titles ) ); //Change the message here

    $message = sprintf( '%s <a href="%s" class="button">%s</a>&nbsp;|&nbsp;<a href="%s" class="button">%s</a>',                 
      esc_html( $added_text ),
      esc_url( wc_get_page_permalink( 'checkout' ) ),
      esc_html__( 'Checkout', 'listingwoo' ),
      esc_url( wc_get_page_permalink( 'cart' ) ),
      esc_html__( 'View Cart', 'listingwoo' ));  
    return $message;
}
add_filter( 'wc_add_to_cart_message_html', 'listingwoo_custom_add_to_cart_message', 10, 2 );

/**
 * Maybe make subscribers product vendors
 */
function listingwoo_maybe_make_users_vendors( $user_id ){
  
  $options = get_option('listingpro_options');
  if ( LISTINGWOO_WCPV == false || $options['listingwoo_redux_create_vendors'] != '1')
    return;

	if (!defined('WC_PRODUCT_VENDORS_TAXONOMY')){
		return;
	}

	if (empty($user_id)) {
		return esc_html__( '<strong>ERROR</strong>: Unable to create the vendor account for this user. User id not found.', 'listingwoo' );
  }
  
	$user_data = get_userdata($user_id);
	$username = $user_data->user_login;
	$email = $user_data->user_email;  

	// Ensure vendor name is unique
	if ( term_exists( $username, WC_PRODUCT_VENDORS_TAXONOMY ) ) {
		$append     = 1;
		$o_username = $username;

		while ( term_exists( $username, WC_PRODUCT_VENDORS_TAXONOMY ) ) {
			$username = $o_username . $append;
			$append ++;
		}
	}

	// Create the new vendor
	$term = wp_insert_term(
		$username,
		WC_PRODUCT_VENDORS_TAXONOMY,
		array(
			'description' => sprintf( __( 'The vendor %s', 'listingwoo' ), $username ),
			'slug'        => sanitize_title( $username )
		)
  );
  
	if ( is_wp_error( $return ) ) {
		return esc_html__( '<strong>ERROR</strong>: Unable to create the vendor account for this user. Please contact the administrator to register your account.', 'listingwoo' );
	} else {
		// Update vendor data
		$vendor_data['paypal'] = ''; // The email used for the account will be used for the payments
		$vendor_data['commission']   = $options['listingwoo_redux_commission']; // The commission is 50% for each order
    
    

    $vendor_data['admins'][]     = $user_id; // The registered account is also the admin of the vendor
    $listingwoo_admin = get_user_by( 'email', $options['listingwoo_redux_admin_email'] );
    if ( ! empty( $listingwoo_admin ) ) {
      $vendor_data['admins'][] = $listingwoo_admin->ID ;
    }    
    
    $vendor_data['enable_bookings']     = 'yes'; // The registered account is also the admin of the vendor

		update_term_meta( $term['term_id'], 'vendor_data', apply_filters( 'wcpv_registration_default_vendor_data', $vendor_data ) );

		// change this user's role to pending vendor
		wp_update_user( apply_filters( 'wcpv_registration_default_user_data', array(
			'ID'   => $user_id,
			'role' => 'wc_product_vendors_manager_vendor',
		) ) );
	}  

}
add_action( 'user_register', 'listingwoo_maybe_make_users_vendors', 10, 1 );

/**
 * Get vendor term
 */
function listingwoo_get_vendor_term_by_id($user_id){
	$terms = get_terms( array(
    'taxonomy' => WC_PRODUCT_VENDORS_TAXONOMY,
    'hide_empty' => false,
	) );

	$args = array(
		'hide_empty' => false, // also retrieve terms which are not used yet
		'meta_key' => 'vendor_data',
		'meta_value' => $user_id,
		'meta_compare' => 'LIKE'
	);
	$terms = get_terms( WC_PRODUCT_VENDORS_TAXONOMY, $args );
	if ( !empty($terms) && is_array($terms) ) {
		// return the first item alone because one user per vendor is my use case
		return $terms[0];
	}
}