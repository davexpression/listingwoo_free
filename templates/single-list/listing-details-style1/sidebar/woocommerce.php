<?php

$product_id = get_post_meta($post->ID, '_wc_product_id_lw', true);

if(empty($product_id))
  return;

echo do_shortcode('[product_page id='.$product_id.']');