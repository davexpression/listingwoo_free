<?php

global $listingpro_options;	


if ( isset( $_POST['woocommerce_integration_nonce_field'] ) && wp_verify_nonce( $_POST['woocommerce_integration_nonce_field'], 'woocommerce_integration_nonce' ) ) {

	$listingwoo_product_id 		= $_POST['listingwoo_product_id'];
	$woocommerceListing 	= $_POST['listingwoo_woocommerceListing'];
	
	if(isset($woocommerceListing) && !empty($woocommerceListing) && isset($woocommerceListing) && !empty($woocommerceListing) ){
		update_post_meta( $woocommerceListing, '_wc_product_id_lw', $listingwoo_product_id);
		update_post_meta( $listingwoo_product_id, '_listingwoo_id', $woocommerceListing);
	}

}

if ( isset( $_POST['woocommerce_del_nonce_field'] ) && wp_verify_nonce( $_POST['woocommerce_del_nonce_field'], 'woocommerce_del_nonce' ) ) {
	$listing_id 		= $_POST['woocommerce_remove_id'];
	if(isset($listing_id) && !empty($listing_id)){
    // print_r($listing_id);
    // print_r( get_post_meta( $listing_id, '_wc_product_id_lw', true ) );
    // wp_die();
    $product_id = get_post_meta( $listing_id, '_wc_product_id_lw', true );
    delete_post_meta($product_id, '_listingwoo_id');
    delete_post_meta($listing_id, '_wc_product_id_lw');
	}
}

$user_ID = get_current_user_id();	
$argsActive = array(
  'author'           => $user_ID,
	'posts_per_page'   => -1,
	'orderby'          => 'date',
	'order'            => 'DESC',
	'post_type'        => 'listing',
	'post_status'      => 'publish',
	'meta_query' =>
	array(
		array(
			'key'     => '_wc_product_id_lw',
			'compare' => 'EXIST'
		)
	),
);

$options = get_option('listingpro_options');
if ( current_user_can( 'manage_options' ) && $options['listingwoo_redux_show_listings_admin'] == '1' ) 
  unset($argsActive['author']);

$Active_array = get_posts( $argsActive );

$args = array(
	'author'   => $user_ID,
	'posts_per_page'   => -1,
	'orderby'          => 'date',
	'order'            => 'DESC',
	'post_type'        => 'listing',
	'post_status'      => 'publish'
);
if ( current_user_can( 'manage_options' ) && $options['listingwoo_redux_show_listings_admin'] == '1' ) 
  unset($args['author']);
$posts_array = get_posts( $args );

$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";


?>
<div class="user-recent-listings-inner tab-pane fade in active lp-timekit-outer" id="resurva_bookings">
	<div class="tab-header">
		<h3><?php echo esc_html__('Bookings', 'listingpro'); ?></h3>
	</div>
	<div class="row lp-list-page-list">
		<div class="col-md-12 col-sm-6 col-xs-12 lp-list-view">
			<div class="resurva-booking">
				<div class="lp-list-view-inner-contianer clearfix">
					<form method="post" id="booking" action="<?php echo esc_attr($actual_link); ?>">
						<a href="#" class="switch-fields"><?php esc_html_e('Add WooCommerce Bookings','listingpro'); ?></a>
						<div class="hidden-items clearfix">
							<div class="row margin-bottom-20">
                
								<div class="col-md-6 col-xs-12">
									<label for="timekitListing"><?php esc_html_e('Select your listing to integrate with WooCommerce','listingpro'); ?></label>
									<?php if(!empty($posts_array)){ ?>
									<select class="select2" name="listingwoo_woocommerceListing" id="timekitListing">
                  <option value=""><?php esc_html_e('Select Listing', 'listingpro') ?></option>
                    <?php										
											foreach ($posts_array as $list) {
											?>
												<option value="<?php echo $list->ID; ?>"><?php echo $list->post_title; ?></option>
											<?php } ?>
									</select>
									<div class="margin-bottom-15"></div>
									<input type="submit" value="Submit" class="lp-review-btn btn-second-hover">
									<?php }else{
										echo esc_html__('You have no published listing.','listingpro');
									} ?>
                </div>
                
              
                <div class="col-md-6 col-xs-12">
                    <label for=""><?php esc_html_e('Select your WooCommerce Product:','listingpro'); ?></label>
                    
                    <?php $products =  listingwoo_get_woocommerce_products(); ?>
                    <?php if( $products->have_posts() ) { ?>

                    <select class="select2" name="listingwoo_product_id" id="listingwoo_product_id">
                      <option value=""><?php esc_html_e('Select Product', 'listingpro') ?></option>
                      <?php										
                        while ($products->have_posts()) : $products->the_post();
                        ?>
                          <option value="<?php echo get_the_ID() ?>"><?php the_title(); ?></option>
                        <?php endwhile ?>
                    </select>

                  

                    <?php } else{ 
                      echo esc_html__('You have no published product.','listingpro');
                    } ?>
                  </div>
                </div>
							
						</div>
						<?php echo wp_nonce_field( 'woocommerce_integration_nonce', 'woocommerce_integration_nonce_field' , true, false ); ?>
					</form>					
				</div>		
			</div>
			<?php if(!empty($Active_array)){ ?>
				<div class="resurva-booking">
					<div class="lp-list-view-inner-contianer clearfix">
					<h3 class="margin-top-0 margin-bottom-30"><?php esc_html_e('WooCommerce Bookings Currently Active On','listingpro'); ?></h3>
						<ul class="padding-left-0">
							<?php						
							foreach ($Active_array as $list) {
                $active_product = get_post_meta( $list->ID, '_wc_product_id_lw', true );
                $active_product_title = get_the_title( $active_product );
							?>
								<li class="clearfix">
									<h3 class="pull-left margin-right-30"><?php echo $list->post_title .' <--> '. $active_product_title; ?></h3>
									<form method="post" id="booking" action="<?php echo esc_attr($actual_link); ?>">
										<input type="hidden" name="woocommerce_remove_id" value="<?php echo $list->ID; ?>" class="lp-review-btn btn-second-hover">
										<span>
											<i class="fa fa-times"></i>
											<input type="submit" value="<?php esc_html_e('Remove','listingpro'); ?>" class="margin-top-10 pull-right">
										</span>
										<?php echo wp_nonce_field( 'woocommerce_del_nonce', 'woocommerce_del_nonce_field' , true, false ); ?>
									</form>	
								</li>
							<?php } ?>						
						</ul>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>