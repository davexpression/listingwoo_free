    <div id="secondary" class="col-md-4 col-sm-4 listing-second-view">
      <div class="side-bar">

        <section id="sidebar">
          <?php
          if(!dynamic_sidebar('default-sidebar')) {
            print 'There is no widget. You should add your widgets into <strong>';
            print 'Default';
            print '</strong> sidebar area on <strong>Appearance => Widgets</strong> of your dashboard. <br/><br/>';
          ?>
              
          <?php } ?>
        </section>
   
      </div> <!-- side-bar -->
    </div> <!-- secondary -->
  </div> <!-- row -->
</div> <!-- container -->

