<?php
/**
 * ListingWoo WooCommerce functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ListingWoo_WooCommerce
 */

define( 'LISTINGWOO_NAME', 'listingwoo');
define( 'LISTINGWOO_VERSION', '1.0');
define( 'LISTINGWOO_WC', class_exists( 'WooCommerce' ) ? TRUE : FALSE );
define( 'LISTINGWOO_WCPV', class_exists( 'WC_Product_Vendors' ) ? TRUE : FALSE );
define( 'LISTINGWOO_WCB', class_exists( 'WC_Bookings' ) ? TRUE : FALSE );
define( 'LISTINGWOO_WCAB', class_exists( 'WC_Accommodation_Bookings_Plugin' ) ? TRUE : FALSE );


if ( ! function_exists( 'listingwoo_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function listingwoo_setup() {

		load_theme_textdomain( 'listingwoo', get_stylesheet_directory() . '/languages' );
	}
endif;
add_action( 'after_setup_theme', 'listingwoo_setup' );

/**
 * Enqueue scripts and styles.
 */
function listingwoo_scripts() {

  $parent_style = 'main-css'; // This is 'listingpro-style' for the Twenty Fifteen theme.

  wp_enqueue_style( $parent_style, get_stylesheet_directory_uri() . '/style.css' );
  wp_enqueue_style( 'child-style',
      get_stylesheet_directory_uri() . '/style.css',
      array( $parent_style ),
      wp_get_theme()->get('Version')
  );
	// wp_enqueue_style( 'listingwoo-woocommerce-style', get_stylesheet_directory_uri() . '/woocommerce.css' );

	wp_enqueue_script( 'listingwoo-navigation', get_stylesheet_directory_uri() . '/js/listingwoo.js', array(), '20151215', true );

}
add_action( 'wp_enqueue_scripts', 'listingwoo_scripts' );


/**
 * Allow Vendor Admins/Manager to access backend
 */
function listingpro_block_admin_access() {
	if ( !current_user_can('administrator')  && isset( $_GET['action'] ) != 'delete' && !(defined('DOING_AJAX') && DOING_AJAX) ) {
	
		$allowed_roles = array('wc_product_vendors_admin_vendor', 'wc_product_vendors_manager_vendor');
		$user = wp_get_current_user();
		$roles = $user->roles;

		if ( false == listingwoo_in_array_any( $allowed_roles, $user->roles ) ) {
			wp_die(esc_html__("You don't have permission to access this page.", "listingwoo"));
		}
	}
}


/**
 * Helper: Advanced in_array()
 *
 * @since    1.0.0
 * @param    string    $needles 
 * @param    string    $haystack 
 */
function listingwoo_in_array_any($needles, $haystack) {
   return !!array_intersect($needles, $haystack);
}

require get_stylesheet_directory() . '/inc/manager.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_stylesheet_directory() . '/inc/template-functions.php';

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
  
  /**
   * Theme Options for this child theme.
   */
  require get_stylesheet_directory() . '/inc/theme-options.php';

  require get_stylesheet_directory() . '/inc/woocommerce.php';
}

